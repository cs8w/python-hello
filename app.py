import os
import socket
from flask import Flask
from flask_mysqldb import MySQL

webapp = Flask(__name__)

webapp.config['MYSQL_HOST'] = 'localhost'
webapp.config['MYSQL_DB'] = 'dbWebapp'
webapp.config['MYSQL_USER'] = 'webappuser'
webapp.config['MYSQL_PASSWORD'] = 'PassWord1'

mysql = MySQL(webapp)

# Hello, world application
@webapp.route("/")
def hello():
    cr = mysql.connection.cursor()
    cr.execute("UPDATE `counter` SET `visits` = `visits` + 1")
    mysql.connection.commit()
    cr.execute("SELECT `visits` FROM `counter` ORDER BY `id` DESC LIMIT 1")
    mysql.connection.commit()
    row = cr.fetchall()
    cr.close()
    for i in row:
      counter1 = i[0]

    html = "<h3>Hello, World!</h3>" \
           "<b>Container:</b> {hostname}<br/>" \
           "<i>Counter:  {counter} </i>"
    return html.format(hostname=socket.gethostname(),counter=counter1)

# Setup database
@webapp.route("/setup")
def dbsetup():
    cr = mysql.connection.cursor()
    #cr.execute("DROP TABLE IF EXISTS `counter`")
    cr.execute("CREATE TABLE IF NOT EXISTS `counter` (`id` INT AUTO_INCREMENT, `visits` INT, INDEX (`id`))")
    mysql.connection.commit()
    html = "<h3>MySQL database checked</h3><br>"
    cr.execute("SELECT * FROM `counter`")
    if cr.rowcount < 1:
        cr.execute("INSERT INTO `counter` (`visits`) VALUES (1)")
        html = html + "<h3>Counter set to 1</h3><br>"
    mysql.connection.commit()
    cr.close()
    return html.format()

@webapp.errorhandler(500)
def handle_500(error):
    return str(error), 500

if __name__ == "__main__":
    webapp.run(host='0.0.0.0')
