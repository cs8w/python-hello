#!/bin/bash

gunicorn wsgi:webapp -b localhost:8080 --workers 3 --access-logfile gunicorn.access.log